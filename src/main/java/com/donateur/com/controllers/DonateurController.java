package com.donateur.com.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.donateur.com.entities.Donateur;
import com.donateur.com.entities.DonationKafala;
import com.donateur.com.entities.Engagement;
import com.donateur.com.services.IDonateurService;

@Controller
@RequestMapping(value = "/donateur")
public class DonateurController {
	
	@Autowired
	private IDonateurService donateurservice;

	@RequestMapping(value = "/")
	public String donateur(Model model) {
		
		List<Donateur> donateurs = donateurservice.selectAll();
		if(donateurs == null) {
			donateurs = new ArrayList<Donateur>();
		}
		model.addAttribute("donateurs", donateurs);
		return "donateur/donateur";
	}
	
	@RequestMapping(value = "/nouveau" , method = RequestMethod.GET)
	public String ajouterdonateur(Model model) {
		
		Donateur donateur = new Donateur();
		model.addAttribute("donateur", donateur);
		return "donateur/ajouterdonateur";
	}
	
	
	@RequestMapping(value = "/ajouterkafala" , method = RequestMethod.GET)
	public String ajouterdonation(Model model) {
		
		DonationKafala donationkafala = new DonationKafala();
		model.addAttribute("donationkafala", donationkafala);
		return "donateur/ajouterkafala";
	}
	
	
	
	@RequestMapping(value = "/engagement" , method = RequestMethod.GET)
	public String ajouterengagemnet(Model model) {
		
		Engagement engagement = new Engagement();
		model.addAttribute("engagement", engagement);
		return "donateur/engagement";
	}
	
	
	
	@RequestMapping(value = "/enregistrer" , method = RequestMethod.POST)
	public String enregistrerDonateur(@ModelAttribute("donateur") Donateur donateur) {
		if (donateur != null) {
			if (donateur.getIddonateur() !=null) {
				donateurservice.update(donateur);
				
			} else {
				donateurservice.save(donateur);
			}
		}
		
		return "redirect:/donateur/";
	}
	
	@RequestMapping(value = "/modifier/{iddonateur}")
	public String modifierdonateur(Model model, @PathVariable Long iddonateur) {
		if (iddonateur != null) {
			Donateur donateur = donateurservice.getByIf(iddonateur);
			if (donateur != null) {
				model.addAttribute("donateur", donateur);
			}
		}
		
		return "donateur/ajouterdonateur";
		
	}
	@RequestMapping(value = "/supprimer/{iddonateur}")
	public String supprimerdonateur(Model model, @PathVariable Long iddonateur) {
		if (iddonateur != null) {
			Donateur donateur = donateurservice.getByIf(iddonateur) ;
			if (donateur != null) {
				donateurservice.remove(iddonateur);
				
			}
		}
		return "redirect:/donateur/";
	}
}
