package com.donateur.com.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.donateur.com.entities.DonationKafala;
import com.donateur.com.services.IDonationKafalaService;;

@Controller
@RequestMapping(value = "/donationkafala")
public class DonationKafalaController {
	
	@Autowired
	private IDonationKafalaService donationkafalaservice;

	@RequestMapping(value = "/")
	public String donationkafala(Model model) {
		
		List<DonationKafala> donationkafalas = donationkafalaservice.selectAll();
		if(donationkafalas == null) {
			donationkafalas = new ArrayList<DonationKafala>();
		}
		model.addAttribute("donationkafalas", donationkafalas);
		return "donationkafala/donationkafala";
	}
	
	
	@RequestMapping(value = "/enregistrer" , method = RequestMethod.POST)
	public String enregistrerDonationKafala(Model model, DonationKafala donationkafala) {
		if (donationkafala != null) {
			if (donationkafala.getIddonationKafala() !=null) {
				donationkafalaservice.update(donationkafala);
				
			} else {
			donationkafalaservice.save(donationkafala);
			}
		}
		
		return "redirect:/donationkafala/donationkafala";
	}
	
	@RequestMapping(value = "/modifier/{iddonationKafala}")
	public String modifierDonationKafala(Model model, @PathVariable Long iddonationKafala) {
		if (iddonationKafala != null) {
			DonationKafala donationkafala = donationkafalaservice.getByIf(iddonationKafala);
			if (donationkafala != null) {
				model.addAttribute("donationkafala", donationkafala);
			}
		}
		
		return "donateur/ajouterkafala";
		
	}
	@RequestMapping(value = "/supprimer/{iddonationkafala}")
	public String supprimerDonationKafala(Model model, @PathVariable Long iddonationKafala) {
		if (iddonationKafala != null) {
			DonationKafala donationkafala = donationkafalaservice.getByIf(iddonationKafala) ;
			if (donationkafala != null) {
				donationkafalaservice.remove(iddonationKafala);
				
			}
		}
		return "redirect:/donationkafala/";
	}
}
