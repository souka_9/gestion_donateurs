package com.donateur.com.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.donateur.com.entities.EnfantEtudiant;
import com.donateur.com.services.IEnfantEtudiantService;

@Controller
@RequestMapping(value = "/enfantetudiant")
public class EnfantEtudiantController {
	
	@Autowired
	private IEnfantEtudiantService enfantetudiantservice;

	@RequestMapping(value = "/")
	public String enfantetudiant(Model model) {
		
		List<EnfantEtudiant> enfantetudiants = enfantetudiantservice.selectAll();
		if(enfantetudiants == null) {
			enfantetudiants = new ArrayList<EnfantEtudiant>();
		}
		model.addAttribute("enfantetudiants", enfantetudiants);
		return "enfantetudiant/enfantetudiant";
	}
	
	@RequestMapping(value = "/nouveau" , method = RequestMethod.GET)
	public String ajouterTuteur(Model model) {
		
		EnfantEtudiant enfantetudiant = new EnfantEtudiant();
		model.addAttribute("enfantetudiant", enfantetudiant);
		return "enfantetudiant/ajouterenfantetudiant";
	}
	
	@RequestMapping(value = "/enregistrer" , method = RequestMethod.POST)
	public String enregistrerTuteur(Model model, EnfantEtudiant enfantetudiant) {
		if (enfantetudiant != null) {
			if (enfantetudiant.getIdbeneficiaire() !=null) {
				enfantetudiantservice.update(enfantetudiant);
				
			} else {
			enfantetudiantservice.save(enfantetudiant);
			}
		}
		
		return "redirect:/enfantetudiant/";
	}
	
	@RequestMapping(value = "/modifier/{idbeneficiaire}")
	public String modifierenfantetudiant(Model model, @PathVariable Long idbeneficiaire) {
		if (idbeneficiaire != null) {
			EnfantEtudiant enfantetudiant = enfantetudiantservice.getByIf(idbeneficiaire);
			if (enfantetudiant != null) {
				model.addAttribute("enfantetudiant", enfantetudiant);
			}
		}
		
		return "enfantetudiant/ajouterenfantetudiant";
		
	}
	@RequestMapping(value = "/supprimer/{idbeneficiaire}")
	public String supprimerenfantetudiant(Model model, @PathVariable Long idbeneficiaire) {
		if (idbeneficiaire != null) {
			EnfantEtudiant enfantetudiant = enfantetudiantservice.getByIf(idbeneficiaire) ;
			if (enfantetudiant != null) {
				enfantetudiantservice.remove(idbeneficiaire);
				
			}
		}
		return "redirect:/enfantetudiant/";
	}
}
