package com.donateur.com.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.donateur.com.entities.Engagement;
import com.donateur.com.entities.DonationKafala;
import com.donateur.com.services.IEngagementService;

@Controller
@RequestMapping(value = "/engagement")
public class EngagementController {
	
	@Autowired
	private IEngagementService engagementservice;

	@RequestMapping(value = "/")
	public String engagement(Model model) {
		
		List<Engagement> engagements = engagementservice.selectAll();
		if(engagements == null) {
			engagements = new ArrayList<Engagement>();
		}
		model.addAttribute("engagements", engagements);
		return "engagement/engagement";
	}
	
	@RequestMapping(value = "/nouveau/{iddonateur}" , method = RequestMethod.GET)
	public String ajouterengagement(Model model) {
		
		Engagement engagement = new Engagement();
		model.addAttribute("engagement", engagement);
		return "engagement/ajouterengagement";
	}
	

	
	
	@RequestMapping(value = "/engagement" , method = RequestMethod.GET)
	public String ajouterengagemnet(Model model) {
		
		Engagement engagement = new Engagement();
		model.addAttribute("engagement", engagement);
		return "engagement/engagement";
	}
	
	
	
	@RequestMapping(value = "/enregistrer/{iddonateur}" , method = RequestMethod.POST)
	public String enregistrerEngagement(@ModelAttribute("engagement") Engagement engagement) {
		if (engagement != null) {
			if (engagement.getIdengagement() !=null) {
				engagementservice.update(engagement);
				
			} else {
				engagementservice.save(engagement);
			}
		}
		
		return "redirect:/engagement/";
	}
	
	@RequestMapping(value = "/modifier/{idengagement}")
	public String modifierengagement(Model model, @PathVariable Long idengagement) {
		if (idengagement != null) {
			Engagement engagement = engagementservice.getByIf(idengagement);
			if (engagement != null) {
				model.addAttribute("engagement", engagement);
			}
		}
		
		return "engagement/ajouterengagement";
		
	}
	@RequestMapping(value = "/supprimer/{idengagement}")
	public String supprimerengagement(Model model, @PathVariable Long idengagement) {
		if (idengagement != null) {
			Engagement engagement = engagementservice.getByIf(idengagement) ;
			if (engagement != null) {
				engagementservice.remove(idengagement);
				
			}
		}
		return "redirect:/engagement/";
	}
}
