package com.donateur.com.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/home")
public class HomeController {
	
	@RequestMapping(value = "/")
	public String home() {
		
		return "home/home";
	}
	
	@RequestMapping(value = "/plain_page")
	public String blankHome() {
		
		return "plain_page/plain_page";
	}

}
