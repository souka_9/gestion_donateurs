package com.donateur.com.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.donateur.com.entities.Tuteur;
import com.donateur.com.entities.Beneficiaire;
import com.donateur.com.services.ITuteurService;

@Controller
@RequestMapping(value = "/tuteur")
public class TuteurController {
	
	@Autowired
	private ITuteurService tuteurservice;

	@RequestMapping(value = "/")
	public String tuteur(Model model) {
		
		List<Tuteur> tuteurs = tuteurservice.selectAll();
		if(tuteurs == null) {
			tuteurs = new ArrayList<Tuteur>();
		}
		model.addAttribute("tuteurs", tuteurs);
		return "tuteur/tuteur";
	}
	
	@RequestMapping(value = "/nouveau" , method = RequestMethod.GET)
	public String ajouterTuteur(Model model) {
		
		Tuteur tuteur = new Tuteur();
		model.addAttribute("tuteur", tuteur);
		return "tuteur/ajoutertuteur";
	}
	
	@RequestMapping(value = "/enregistrer" , method = RequestMethod.POST)
	public String enregistrerTuteur(Model model, Tuteur tuteur) {
		if (tuteur != null) {
			if (tuteur.getIdbeneficiaire() !=null) {
				tuteurservice.update(tuteur);
				
			} else {
			tuteurservice.save(tuteur);
			}
		}
		
		return "redirect:/tuteur/";
	}
	
	@RequestMapping(value = "/modifier/{idbeneficiaire}")
	public String modifiertuteur(Model model, @PathVariable Long idbeneficiaire) {
		if (idbeneficiaire != null) {
			Tuteur tuteur = tuteurservice.getByIf(idbeneficiaire);
			if (tuteur != null) {
				model.addAttribute("tuteur", tuteur);
			}
		}
		
		return "tuteur/ajoutertuteur";
		
	}
	@RequestMapping(value = "/supprimer/{idbeneficiaire}")
	public String supprimertuteur(Model model, @PathVariable Long idbeneficiaire) {
		if (idbeneficiaire != null) {
			Tuteur tuteur = tuteurservice.getByIf(idbeneficiaire) ;
			if (tuteur != null) {
				tuteurservice.remove(idbeneficiaire);
				
			}
		}
		return "redirect:/tuteur/";
	}
}
