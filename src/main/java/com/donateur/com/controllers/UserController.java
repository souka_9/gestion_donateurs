package com.donateur.com.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.donateur.com.entities.User;
import com.donateur.com.services.IUserService;

@Controller
@RequestMapping(value = "/user")
public class UserController {
	
	@Autowired
	private IUserService userservice;

	@RequestMapping(value = "/")
	public String user(Model model) {
		
		List<User> users = userservice.selectAll();
		if(users == null) {
			users = new ArrayList<User>();
		}
		model.addAttribute("users", users);
		return "user/user";
	}
	
	@RequestMapping(value = "/nouveau" , method = RequestMethod.GET)
	public String ajouterUser(Model model) {
		
		User user = new User();
		model.addAttribute("user", user);
		return "user/ajouteruser";
	}
	
	@RequestMapping(value = "/enregistrer" , method = RequestMethod.POST)
	public String enregistrerUser(Model model, User user) {
		if (user != null) {
			if (user.getIduser() !=null) {
				userservice.update(user);
				
			} else {
			userservice.save(user);
			}
		}
		
		return "redirect:/user/";
	}
	
	@RequestMapping(value = "/modifier/{iduser}")
	public String modifierUser(Model model, @PathVariable Long iduser) {
		if (iduser != null) {
			User user = userservice.getByIf(iduser);
			if (user != null) {
				model.addAttribute("user", user);
			}
		}
		
		return "user/ajouteruser";
		
	}
	@RequestMapping(value = "/supprimer/{iduser}")
	public String supprimerUser(Model model, @PathVariable Long iduser) {
		if (iduser != null) {
			User user = userservice.getByIf(iduser) ;
			if (user != null) {
				userservice.remove(iduser);
				
			}
		}
		return "redirect:/user/";
	}
}
