package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.AffectationRemiseAide;

public interface IAffectationRemiseAideDao extends IGenericDao<AffectationRemiseAide> {

}
