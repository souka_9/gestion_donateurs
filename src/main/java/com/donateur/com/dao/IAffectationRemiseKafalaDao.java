package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.AffectationRemiseKafala;

public interface IAffectationRemiseKafalaDao extends IGenericDao<AffectationRemiseKafala> {

}
