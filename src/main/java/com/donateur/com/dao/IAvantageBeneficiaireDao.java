package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.AvantageBeneficiaire;

public interface IAvantageBeneficiaireDao extends IGenericDao<AvantageBeneficiaire> {

}
