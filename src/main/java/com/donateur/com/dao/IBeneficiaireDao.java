package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.Beneficiaire;

public interface IBeneficiaireDao extends IGenericDao<Beneficiaire> {

}
