package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.Donateur;

public interface IDonateurDao extends IGenericDao<Donateur> {

}
