package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.DonationAide;


public interface IDonationAideDao extends IGenericDao<DonationAide> {

}
