package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.DonationKafala;

public interface IDonationKafalaDao extends IGenericDao<DonationKafala> {

}
