package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.EnfantEtudiant;

public interface IEnfantEtudiantDao extends IGenericDao<EnfantEtudiant> {

}
