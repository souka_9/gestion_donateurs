package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.Engagement;

public interface IEngagementDao extends IGenericDao<Engagement> {

}
