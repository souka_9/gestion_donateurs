package com.donateur.com.dao;

import java.util.List;

public interface IGenericDao<E> {
	
   public E save(E entity);
	
	public E update(E entity);
	
	public List<E> selectAll();
	
	public E getByIf(Long id);
	
	public void remove(Long id);

	public List<E> selectAll(String sortField, String sort);
	
	public E finfOne(String paramName, Object paramValue);
	
	public E finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);
	

}
