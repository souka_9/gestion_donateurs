package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.ModeleReporting;

public interface IModeleReportingDao extends IGenericDao<ModeleReporting> {

}
