package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.Resultat;

public interface IResultatDao extends IGenericDao<Resultat> {

}
