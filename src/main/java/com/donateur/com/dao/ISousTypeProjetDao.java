package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.SousTypeProjet;

public interface ISousTypeProjetDao extends IGenericDao<SousTypeProjet> {

}
