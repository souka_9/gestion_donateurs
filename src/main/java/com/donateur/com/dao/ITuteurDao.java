package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.Tuteur;

public interface ITuteurDao extends IGenericDao<Tuteur> {

}
