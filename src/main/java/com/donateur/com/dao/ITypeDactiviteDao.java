package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.TypeDactivite;

public interface ITypeDactiviteDao extends IGenericDao<TypeDactivite> {

}
