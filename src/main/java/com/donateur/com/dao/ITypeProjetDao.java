package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.TypeProjet;

public interface ITypeProjetDao extends IGenericDao<TypeProjet> {

}
