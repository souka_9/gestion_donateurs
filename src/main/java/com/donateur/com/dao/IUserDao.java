package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.User;

public interface IUserDao extends IGenericDao<User> {

}
