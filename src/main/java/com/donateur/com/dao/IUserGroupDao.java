package com.donateur.com.dao;

import com.donateur.com.dao.IGenericDao;
import com.donateur.com.entities.UserGroup;

public interface IUserGroupDao extends IGenericDao<UserGroup> {

}
