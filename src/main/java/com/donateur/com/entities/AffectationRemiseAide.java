package com.donateur.com.entities;



import java.io.Serializable;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class AffectationRemiseAide implements Serializable{
	
	@Id
	@GeneratedValue
   private Long idAffeReAide;

   private int nmbrBenef;

   private String statutRemise;
   
  

public Long getIdAffeReAide() {
	return idAffeReAide;
}

public void setIdAffeReAide(Long idAffeReAide) {
	this.idAffeReAide = idAffeReAide;
}

public int getNmbrBenef() {
	return nmbrBenef;
}

public void setNmbrBenef(int nmbrBenef) {
	this.nmbrBenef = nmbrBenef;
}

public String getStatutRemise() {
	return statutRemise;
}

public void setStatutRemise(String statutRemise) {
	this.statutRemise = statutRemise;
}


   
}