package com.donateur.com.entities;

import java.io.Serializable;

import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class AffectationRemiseKafala implements Serializable{
	
	@Id
	@GeneratedValue
   private long idAffeReKafala;
   /** @pdOid 450caead-3bda-4510-b29a-1b56339368bb */
   private int montantAffecte;
   /** @pdOid 50488743-c00b-4a84-bbc2-c42faee0bceb */
   private int nbrBnef;
   /** @pdOid 4dce299a-c4f0-483f-97db-4bee4c974942 */
   private String statutRemise;
   
   

public long getIdAffeReKafala() {
	return idAffeReKafala;
}

public void setIdAffeReKafala(long idAffeReKafala) {
	this.idAffeReKafala = idAffeReKafala;
}

public int getMontantAffecte() {
	return montantAffecte;
}

public void setMontantAffecte(int montantAffecte) {
	this.montantAffecte = montantAffecte;
}

public int getNbrBnef() {
	return nbrBnef;
}

public void setNbrBnef(int nbrBnef) {
	this.nbrBnef = nbrBnef;
}

public String getStatutRemise() {
	return statutRemise;
}

public void setStatutRemise(String statutRemise) {
	this.statutRemise = statutRemise;
}

     
   

}