/***********************************************************************
 * Module:  AvantageBeneficiaire.java
 * Author:  DELL
 * Purpose: Defines the Class AvantageBeneficiaire
 ***********************************************************************/

package com.donateur.com.entities;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class AvantageBeneficiaire implements Serializable{
	
	@Id
	@GeneratedValue
	private int idAvantage;
   

   
   public int getIdAvantage() {
	return idAvantage;
}

public void setIdAvantage(int idAvantage) {
	this.idAvantage = idAvantage;
}



}