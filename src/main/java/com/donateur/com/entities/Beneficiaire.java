
package com.donateur.com.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Beneficiaire implements Serializable{
	
    private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long idbeneficiaire;
   
   private String typeBenef;

   private Date dateNaissance;

   private String nom;

   private String prenom;

   private String nomDeFamille;

   private Boolean statusAI;

   
   private Boolean statutSortie;
   
   @ManyToMany(cascade = { CascadeType.ALL })
   @JoinTable(
       name = "Beneficiaire_Projet", 
       joinColumns = { @JoinColumn(name = "idbeneficiaire") }, 
       inverseJoinColumns = { @JoinColumn(name = "idTypeProjet") }
   )
   Set<TypeProjet> projets = new HashSet<TypeProjet>();
   
   @ManyToOne
   @JoinColumn(name="iddonationKafala")
   private DonationKafala donationkafala;
   
   
   public Beneficiaire() {}

   private String photo;
public Long getIdbeneficiaire() {
	return idbeneficiaire;
}
public void setIdbeneficiaire(Long idbeneficiaire) {
	this.idbeneficiaire = idbeneficiaire;
}
public String getTypeBenef() {
	return typeBenef;
}
public void setTypeBenef(String typeBenef) {
	this.typeBenef = typeBenef;
}
public Date getDateNaissance() {
	return dateNaissance;
}
public void setDateNaissance(Date dateNaissance) {
	this.dateNaissance = dateNaissance;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getPrenom() {
	return prenom;
}
public void setPrenom(String prenom) {
	this.prenom = prenom;
}
public String getNomDeFamille() {
	return nomDeFamille;
}
public void setNomDeFamille(String nomDeFamille) {
	this.nomDeFamille = nomDeFamille;
}

public Boolean getStatusAI() {
	return statusAI;
}
public void setStatusAI(Boolean statusAI) {
	this.statusAI = statusAI;
}
public Boolean getStatutSortie() {
	return statutSortie;
}
public void setStatutSortie(Boolean statutSortie) {
	this.statutSortie = statutSortie;
}
public Set<TypeProjet> getProjets() {
	return projets;
}
public void setProjets(Set<TypeProjet> projets) {
	this.projets = projets;
}
public DonationKafala getDonationkafala() {
	return donationkafala;
}
public void setDonationkafala(DonationKafala donationkafala) {
	this.donationkafala = donationkafala;
}
public String getPhoto() {
	return photo;
}
public void setPhoto(String photo) {
	this.photo = photo;
}

}