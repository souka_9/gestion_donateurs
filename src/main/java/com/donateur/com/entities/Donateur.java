
package com.donateur.com.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Donateur implements Serializable{
   
	@Id
	@GeneratedValue
   private Long iddonateur;

	private String typeDonateur;

	private Boolean statuDonateurRIr;

	private String designation;

	private String nomPrenomContact;

	private String tel;

   private String email;

   private String commentaire;

   private Boolean statutActifInactif;
   
   
   @ManyToMany(cascade = { CascadeType.ALL })
   @JoinTable(
       name = "Donateur_Project", 
       joinColumns = { @JoinColumn(name = "iddonateur") }, 
       inverseJoinColumns = { @JoinColumn(name = "idTypeProjet") }
   )
   Set<TypeProjet> projects = new HashSet<TypeProjet>();
 

public Long getIddonateur() {
	return iddonateur;
}

public void setIddonateur(Long iddonateur) {
	this.iddonateur = iddonateur;
}

public String getTypeDonateur() {
	return typeDonateur;
}

public void setTypeDonateur(String typeDonateur) {
	this.typeDonateur = typeDonateur;
}




public Boolean getStatuDonateurRIr() {
	return statuDonateurRIr;
}

public void setStatuDonateurRIr(Boolean statuDonateurRIr) {
	this.statuDonateurRIr = statuDonateurRIr;
}

public void setStatutActifInactif(Boolean statutActifInactif) {
	this.statutActifInactif = statutActifInactif;
}

public String getNomPrenomContact() {
	return nomPrenomContact;
}

public void setNomPrenomContact(String nomPrenomContact) {
	this.nomPrenomContact = nomPrenomContact;
}


public String getDesignation() {
	return designation;
}

public void setDesignation(String designation) {
	this.designation = designation;
}


public String getTel() {
	return tel;
}

public void setTel(String tel) {
	this.tel = tel;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getCommentaire() {
	return commentaire;
}

public void setCommentaire(String commentaire) {
	this.commentaire = commentaire;
}

public Boolean getStatutActifInactif() {
	return statutActifInactif;
}


}


