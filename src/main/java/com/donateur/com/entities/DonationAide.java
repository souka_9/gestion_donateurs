
package com.donateur.com.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class DonationAide implements Serializable{

	@Id
	@GeneratedValue
	private Long iddonationAide;

	private String typeDonation;
	
   private String typeAide;

   private String typeDon;

   private String designationDon;
   
   private String typePaiement;
   
   private String statutRapport;
   
   public AffectationRemiseAide association6;

   @ElementCollection
   public List<ModeleReporting> modeleReporting;
   
   
   public Long getIddonationAide() {
	return iddonationAide;
}

public void setIddonationAide(Long iddonationAide) {
	this.iddonationAide = iddonationAide;
}

public String getTypeDonation() {
	return typeDonation;
}

public void setTypeDonation(String typeDonation) {
	this.typeDonation = typeDonation;
}

public String getTypeAide() {
	return typeAide;
}

public void setTypeAide(String typeAide) {
	this.typeAide = typeAide;
}

public String getTypeDon() {
	return typeDon;
}

public void setTypeDon(String typeDon) {
	this.typeDon = typeDon;
}

public String getDesignationDon() {
	return designationDon;
}

public void setDesignationDon(String designationDon) {
	this.designationDon = designationDon;
}

public String getTypePaiement() {
	return typePaiement;
}

public void setTypePaiement(String typePaiement) {
	this.typePaiement = typePaiement;
}

public String getStatutRapport() {
	return statutRapport;
}

public void setStatutRapport(String statutRapport) {
	this.statutRapport = statutRapport;
}

public AffectationRemiseAide getAssociation6() {
	return association6;
}

public void setAssociation6(AffectationRemiseAide association6) {
	this.association6 = association6;
}

/** @pdGenerated default getter */
   public java.util.Collection<ModeleReporting> getModeleReporting() {
      if (modeleReporting == null)
         modeleReporting = (List<ModeleReporting>) new java.util.HashSet<ModeleReporting>();
      return modeleReporting;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorModeleReporting() {
      if (modeleReporting == null)
         modeleReporting = (List<ModeleReporting>) new java.util.HashSet<ModeleReporting>();
      return modeleReporting.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newModeleReporting */
   public void setModeleReporting(java.util.Collection<ModeleReporting> newModeleReporting) {
      removeAllModeleReporting();
      for (java.util.Iterator iter = newModeleReporting.iterator(); iter.hasNext();)
         addModeleReporting((ModeleReporting)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newModeleReporting */
   public void addModeleReporting(ModeleReporting newModeleReporting) {
      if (newModeleReporting == null)
         return;
      if (this.modeleReporting == null)
         this.modeleReporting = (List<ModeleReporting>) new java.util.HashSet<ModeleReporting>();
      if (!this.modeleReporting.contains(newModeleReporting))
         this.modeleReporting.add(newModeleReporting);
   }
   
   /** @pdGenerated default remove
     * @param oldModeleReporting */
   public void removeModeleReporting(ModeleReporting oldModeleReporting) {
      if (oldModeleReporting == null)
         return;
      if (this.modeleReporting != null)
         if (this.modeleReporting.contains(oldModeleReporting))
            this.modeleReporting.remove(oldModeleReporting);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllModeleReporting() {
      if (modeleReporting != null)
         modeleReporting.clear();
   }

}