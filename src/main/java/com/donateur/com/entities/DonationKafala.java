
package com.donateur.com.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity
public class DonationKafala implements Serializable{
   
	@Id
	@GeneratedValue
   private Long iddonationKafala;
 
	private Date dateDon;
   
	private String typeKafala;
   
	private String typePaiement;
   
	private int montant;
   
	private String statutRapportDonateur;
   
   public AffectationRemiseKafala association4;
  

   @OneToMany(mappedBy="donationkafala")
   private Set<Beneficiaire> beneficiaire;
   
  

   public ModeleReporting[] modeleReporting;
public Long getIddonationKafala() {
	return iddonationKafala;
}

public Set<Beneficiaire> getBeneficiaire() {
	return beneficiaire;
}
public void setBeneficiaire(Set<Beneficiaire> beneficiaire) {
	this.beneficiaire = beneficiaire;
}
public void setIddonationKafala(Long iddonationKafala) {
	this.iddonationKafala = iddonationKafala;
}
public Date getDateDon() {
	return dateDon;
}
public void setDateDon(Date dateDon) {
	this.dateDon = dateDon;
}
public String getTypeKafala() {
	return typeKafala;
}
public void setTypeKafala(String typeKafala) {
	this.typeKafala = typeKafala;
}
public String getTypePaiement() {
	return typePaiement;
}
public void setTypePaiement(String typePaiement) {
	this.typePaiement = typePaiement;
}
public int getMontant() {
	return montant;
}
public void setMontant(int montant) {
	this.montant = montant;
}
public String getStatutRapportDonateur() {
	return statutRapportDonateur;
}
public void setStatutRapportDonateur(String statutRapportDonateur) {
	this.statutRapportDonateur = statutRapportDonateur;
}
public AffectationRemiseKafala getAssociation4() {
	return association4;
}
public void setAssociation4(AffectationRemiseKafala association4) {
	this.association4 = association4;
}
public ModeleReporting[] getModeleReporting() {
	return modeleReporting;
}
public void setModeleReporting(ModeleReporting[] modeleReporting) {
	this.modeleReporting = modeleReporting;
}

   
}