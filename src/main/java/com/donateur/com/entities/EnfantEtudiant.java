
package com.donateur.com.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "idbeneficiaire")
public class EnfantEtudiant extends Beneficiaire {

    private static final long serialVersionUID = 1L;

	
	@GeneratedValue
	private Long idEnfantEtudiant;

	private String nom_E;

	private String email;
   
   



public Long getIdEnfantEtudiant() {
		return idEnfantEtudiant;
	}

	public void setIdEnfantEtudiant(Long idEnfantEtudiant) {
		this.idEnfantEtudiant = idEnfantEtudiant;
	}

public String getNom_E() {
	return nom_E;
}

public void setNom_E(String nom) {
	this.nom_E = nom;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}


   
}