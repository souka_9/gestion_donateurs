

package com.donateur.com.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Engagement implements Serializable{
	
	@Id
	@GeneratedValue
   private Long idengagement;

   private int nmbrBenef;

   
  private int montantMensuel;

   private String typeSsProjet;


   public Engagement() {}
   
   
public Long getIdengagement() {
	return idengagement;
}

public void setIdengagement(Long idengagement) {
	this.idengagement = idengagement;
}

public int getNmbrBenef() {
	return nmbrBenef;
}

public void setNmbrBenef(int nmbrBenef) {
	this.nmbrBenef = nmbrBenef;
}

public int getMontantMensuel() {
	return montantMensuel;
}

public void setMontantMensuel(int montantMensuel) {
	this.montantMensuel = montantMensuel;
}

public String getTypeSsProjet() {
	return typeSsProjet;
}

public void setTypeSsProjet(String typeSsProjet) {
	this.typeSsProjet = typeSsProjet;
}
   

 

}