/***********************************************************************
 * Module:  ModeleReporting.java
 * Author:  DELL
 * Purpose: Defines the Class ModeleReporting
 ***********************************************************************/
package com.donateur.com.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class ModeleReporting implements Serializable{

	@Id
	@GeneratedValue
	private int idModele;

	private String format;
   /** @pdOid 5f2bc28c-6b7d-4905-a53b-fae55e46c50e */
   private String designation;
   /** @pdOid e847d913-7843-4ab2-acac-4cdcd75f372c */
   private String contenu;
   /** @pdOid 675b5be7-c16f-4c79-b247-0ac3a5964a85 */
   private String version;

}