/***********************************************************************
 * Module:  Resultat.java
 * Author:  DELL
 * Purpose: Defines the Class Resultat
 ***********************************************************************/
package com.donateur.com.entities;

import java.io.Serializable;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Resultat implements Serializable{
	@Id
	@GeneratedValue
	private long idresultat; 
	
   private String matiere;
  
   private int note;

   private Long moyen;
   
public long getIdresultat() {
	return idresultat;
}
public void setIdresultat(long idresultat) {
	this.idresultat = idresultat;
}
public String getMatiere() {
	return matiere;
}
public void setMatiere(String matiere) {
	this.matiere = matiere;
}
public int getNote() {
	return note;
}
public void setNote(int note) {
	this.note = note;
}
public Long getMoyen() {
	return moyen;
}
public void setMoyen(Long moyen) {
	this.moyen = moyen;
}
   
   

}