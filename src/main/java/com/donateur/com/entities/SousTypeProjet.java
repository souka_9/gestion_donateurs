
package com.donateur.com.entities;

import java.io.Serializable;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class SousTypeProjet implements Serializable{

	@Id
	@GeneratedValue
	private Long idSsTypeProjet;

	private String designation;
	
	
	 @ManyToOne
	    @JoinColumn(name="idprojet", nullable=false)
	    private TypeProjet typeprojet;
	 
	    public SousTypeProjet() {}
	
	
public TypeProjet getTypeprojet() {
			return typeprojet;
		}


		public void setTypeprojet(TypeProjet typeprojet) {
			this.typeprojet = typeprojet;
		}


public Long getIdSsTypeProjet() {
	return idSsTypeProjet;
}
public void setIdSsTypeProjet(Long idSsTypeProjet) {
	this.idSsTypeProjet = idSsTypeProjet;
}
public String getDesignation() {
	return designation;
}
public void setDesignation(String designation) {
	this.designation = designation;
}

   
}