
package com.donateur.com.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "idbeneficiaire")
public class Tuteur extends Beneficiaire {
	
    private static final long serialVersionUID = 1L;

	
	@GeneratedValue
	private Long idtuteur;

	private String cin;
	
   private String adresse;

   private String profession;

   private String etatFami;

   private int tel;

   private int nmbEnfant;

   
public Long getIdtuteur() {
	return idtuteur;
}
public void setIdtuteur(Long idtuteur) {
	this.idtuteur = idtuteur;
}
public String getCin() {
	return cin;
}
public void setCin(String cin) {
	this.cin = cin;
}

public String getAdresse() {
	return adresse;
}
public void setAdresse(String adresse) {
	this.adresse = adresse;
}
public String getProfession() {
	return profession;
}
public void setProfession(String profession) {
	this.profession = profession;
}
public String getEtatFami() {
	return etatFami;
}
public void setEtatFami(String etatFami) {
	this.etatFami = etatFami;
}
public int getTel() {
	return tel;
}
public void setTel(int tel) {
	this.tel = tel;
}
public int getNmbEnfant() {
	return nmbEnfant;
}
public void setNmbEnfant(int nmbEnfant) {
	this.nmbEnfant = nmbEnfant;
}
   
   

}