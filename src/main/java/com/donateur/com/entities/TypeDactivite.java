/***********************************************************************
 * Module:  TypeDactivite.java
 * Author:  DELL
 * Purpose: Defines the Class TypeDactivite
 ***********************************************************************/
package com.donateur.com.entities;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TypeDactivite {
	@Id
	@GeneratedValue
	private int idActivite;

	private String designation;

}