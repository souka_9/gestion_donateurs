/***********************************************************************
 * Module:  TypeProjet.java
 * Author:  DELL
 * Purpose: Defines the Class TypeProjet
 ***********************************************************************/
package com.donateur.com.entities;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class TypeProjet {
	@Id
	@GeneratedValue
	private Long idTypeProjet;
 
   private String designation;
   
   @ManyToMany(mappedBy = "projets")
   private Set<Beneficiaire> beneficiaires = new HashSet<Beneficiaire>();
   
   
	@ManyToMany(mappedBy = "projects")
   private Set<Donateur> donateurs = new HashSet<Donateur>();
 

public Long getIdTypeProjet() {
	return idTypeProjet;
}

public void setIdTypeProjet(Long idTypeProjet) {
	this.idTypeProjet = idTypeProjet;
}

public String getDesignation() {
	return designation;
}

public void setDesignation(String designation) {
	this.designation = designation;
}

public Set<Beneficiaire> getBeneficiaires() {
	return beneficiaires;
}

public void setBeneficiaires(Set<Beneficiaire> beneficiaires) {
	this.beneficiaires = beneficiaires;
}

public Set<Donateur> getDonateurs() {
	return donateurs;
}

public void setDonateurs(Set<Donateur> donateurs) {
	this.donateurs = donateurs;
}


   
   
 
}