
package com.donateur.com.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User implements Serializable{
	@Id
	@GeneratedValue
	private Long iduser;

   private String nom;
   private String email;
   private String prenom;
   private String tel;
   private String password;
   private String login;
   private String groupe;
   

public Long getIduser() {
	return iduser;
}
public void setIduser(Long iduser) {
	this.iduser = iduser;
}
public String getGroupe() {
	return groupe;
}
public void setGroupe(String groupe) {
	this.groupe = groupe;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPrenom() {
	return prenom;
}
public void setPrenom(String prenom) {
	this.prenom = prenom;
}
public String getTel() {
	return tel;
}
public void setTel(String tel) {
	this.tel = tel;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getLogin() {
	return login;
}
public void setLogin(String login) {
	this.login = login;
}
  

}