/***********************************************************************
 * Module:  UserGroup.java
 * Author:  DELL
 * Purpose: Defines the Class UserGroup
 ***********************************************************************/
package com.donateur.com.entities;

import java.io.Serializable;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class UserGroup implements Serializable{

	@Id
	@GeneratedValue
   private Long idgroup;

	private String nom;
	
public Long getIdgroup() {
	return idgroup;
}
public void setIdgroup(Long idgroup) {
	this.idgroup = idgroup;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}

   
}