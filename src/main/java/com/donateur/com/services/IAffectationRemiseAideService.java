package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.AffectationRemiseAide;


public interface IAffectationRemiseAideService {
	
public AffectationRemiseAide save(AffectationRemiseAide entity);
	
	public AffectationRemiseAide update(AffectationRemiseAide entity);
	
	public List<AffectationRemiseAide> selectAll();
	
	public AffectationRemiseAide getByIf(Long id);
	
	public void remove(Long id);

	public List<AffectationRemiseAide> selectAll(String sortField, String sort);
	
	public AffectationRemiseAide finfOne(String paramName, Object paramValue);
	
	public AffectationRemiseAide finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
