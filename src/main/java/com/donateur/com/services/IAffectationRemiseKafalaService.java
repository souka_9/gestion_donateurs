package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.AffectationRemiseKafala;


public interface IAffectationRemiseKafalaService {
	
public AffectationRemiseKafala save(AffectationRemiseKafala entity);
	
	public AffectationRemiseKafala update(AffectationRemiseKafala entity);
	
	public List<AffectationRemiseKafala> selectAll();
	
	public AffectationRemiseKafala getByIf(Long id);
	
	public void remove(Long id);

	public List<AffectationRemiseKafala> selectAll(String sortField, String sort);
	
	public AffectationRemiseKafala finfOne(String paramName, Object paramValue);
	
	public AffectationRemiseKafala finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
