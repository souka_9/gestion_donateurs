package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.AvantageBeneficiaire;


public interface IAvantageBeneficiaireService {
	
public AvantageBeneficiaire save(AvantageBeneficiaire entity);
	
	public AvantageBeneficiaire update(AvantageBeneficiaire entity);
	
	public List<AvantageBeneficiaire> selectAll();
	
	public AvantageBeneficiaire getByIf(Long id);
	
	public void remove(Long id);

	public List<AvantageBeneficiaire> selectAll(String sortField, String sort);
	
	public AvantageBeneficiaire finfOne(String paramName, Object paramValue);
	
	public AvantageBeneficiaire finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
