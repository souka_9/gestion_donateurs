package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.Beneficiaire;


public interface IBeneficiaireService {
	
public Beneficiaire save(Beneficiaire entity);
	
	public Beneficiaire update(Beneficiaire entity);
	
	public List<Beneficiaire> selectAll();
	
	public Beneficiaire getByIf(Long id);
	
	public void remove(Long id);

	public List<Beneficiaire> selectAll(String sortField, String sort);
	
	public Beneficiaire finfOne(String paramName, Object paramValue);
	
	public Beneficiaire finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
