package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.Donateur;


public interface IDonateurService {
	
public Donateur save(Donateur entity);
	
	public Donateur update(Donateur entity);
	
	public List<Donateur> selectAll();
	
	public Donateur getByIf(Long id);
	
	public void remove(Long id);

	public List<Donateur> selectAll(String sortField, String sort);
	
	public Donateur finfOne(String paramName, Object paramValue);
	
	public Donateur finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
