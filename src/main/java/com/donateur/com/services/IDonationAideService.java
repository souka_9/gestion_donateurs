package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.DonationAide;


public interface IDonationAideService {
	
public DonationAide save(DonationAide entity);
	
	public DonationAide update(DonationAide entity);
	
	public List<DonationAide> selectAll();
	
	public DonationAide getByIf(Long id);
	
	public void remove(Long id);

	public List<DonationAide> selectAll(String sortField, String sort);
	
	public DonationAide finfOne(String paramName, Object paramValue);
	
	public DonationAide finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
