package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.DonationKafala;


public interface IDonationKafalaService {
	
public DonationKafala save(DonationKafala entity);
	
	public DonationKafala update(DonationKafala entity);
	
	public List<DonationKafala> selectAll();
	
	public DonationKafala getByIf(Long id);
	
	public void remove(Long id);

	public List<DonationKafala> selectAll(String sortField, String sort);
	
	public DonationKafala finfOne(String paramName, Object paramValue);
	
	public DonationKafala finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
