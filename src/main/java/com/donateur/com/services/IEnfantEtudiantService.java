package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.EnfantEtudiant;


public interface IEnfantEtudiantService {
	
public EnfantEtudiant save(EnfantEtudiant entity);
	
	public EnfantEtudiant update(EnfantEtudiant entity);
	
	public List<EnfantEtudiant> selectAll();
	
	public EnfantEtudiant getByIf(Long id);
	
	public void remove(Long id);

	public List<EnfantEtudiant> selectAll(String sortField, String sort);
	
	public EnfantEtudiant finfOne(String paramName, Object paramValue);
	
	public EnfantEtudiant finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
