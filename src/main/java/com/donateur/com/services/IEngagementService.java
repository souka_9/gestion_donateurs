package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.Engagement;


public interface IEngagementService {
	
public Engagement save(Engagement entity);
	
	public Engagement update(Engagement entity);
	
	public List<Engagement> selectAll();
	
	public Engagement getByIf(Long id);
	
	public void remove(Long id);

	public List<Engagement> selectAll(String sortField, String sort);
	
	public Engagement finfOne(String paramName, Object paramValue);
	
	public Engagement finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
