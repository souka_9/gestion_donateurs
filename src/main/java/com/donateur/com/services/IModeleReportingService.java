package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.ModeleReporting;


public interface IModeleReportingService {
	
public ModeleReporting save(ModeleReporting entity);
	
	public ModeleReporting update(ModeleReporting entity);
	
	public List<ModeleReporting> selectAll();
	
	public ModeleReporting getByIf(Long id);
	
	public void remove(Long id);

	public List<ModeleReporting> selectAll(String sortField, String sort);
	
	public ModeleReporting finfOne(String paramName, Object paramValue);
	
	public ModeleReporting finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
