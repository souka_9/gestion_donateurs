package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.Resultat;


public interface IResultatService {
	
public Resultat save(Resultat entity);
	
	public Resultat update(Resultat entity);
	
	public List<Resultat> selectAll();
	
	public Resultat getByIf(Long id);
	
	public void remove(Long id);

	public List<Resultat> selectAll(String sortField, String sort);
	
	public Resultat finfOne(String paramName, Object paramValue);
	
	public Resultat finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
