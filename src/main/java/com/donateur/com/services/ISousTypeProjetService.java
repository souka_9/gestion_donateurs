package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.SousTypeProjet;


public interface ISousTypeProjetService {
	
public SousTypeProjet save(SousTypeProjet entity);
	
	public SousTypeProjet update(SousTypeProjet entity);
	
	public List<SousTypeProjet> selectAll();
	
	public SousTypeProjet getByIf(Long id);
	
	public void remove(Long id);

	public List<SousTypeProjet> selectAll(String sortField, String sort);
	
	public SousTypeProjet finfOne(String paramName, Object paramValue);
	
	public SousTypeProjet finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
