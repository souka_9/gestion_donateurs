package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.Tuteur;


public interface ITuteurService {
	
public Tuteur save(Tuteur entity);
	
	public Tuteur update(Tuteur entity);
	
	public List<Tuteur> selectAll();
	
	public Tuteur getByIf(Long id);
	
	public void remove(Long id);

	public List<Tuteur> selectAll(String sortField, String sort);
	
	public Tuteur finfOne(String paramName, Object paramValue);
	
	public Tuteur finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
