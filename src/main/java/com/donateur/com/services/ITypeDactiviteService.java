package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.TypeDactivite;


public interface ITypeDactiviteService {
	
public TypeDactivite save(TypeDactivite entity);
	
	public TypeDactivite update(TypeDactivite entity);
	
	public List<TypeDactivite> selectAll();
	
	public TypeDactivite getByIf(Long id);
	
	public void remove(Long id);

	public List<TypeDactivite> selectAll(String sortField, String sort);
	
	public TypeDactivite finfOne(String paramName, Object paramValue);
	
	public TypeDactivite finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
