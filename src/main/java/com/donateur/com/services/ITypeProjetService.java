package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.TypeProjet;


public interface ITypeProjetService {
	
public TypeProjet save(TypeProjet entity);
	
	public TypeProjet update(TypeProjet entity);
	
	public List<TypeProjet> selectAll();
	
	public TypeProjet getByIf(Long id);
	
	public void remove(Long id);

	public List<TypeProjet> selectAll(String sortField, String sort);
	
	public TypeProjet finfOne(String paramName, Object paramValue);
	
	public TypeProjet finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
