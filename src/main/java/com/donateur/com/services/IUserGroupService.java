package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.UserGroup;


public interface IUserGroupService {
	
public UserGroup save(UserGroup entity);
	
	public UserGroup update(UserGroup entity);
	
	public List<UserGroup> selectAll();
	
	public UserGroup getByIf(Long id);
	
	public void remove(Long id);

	public List<UserGroup> selectAll(String sortField, String sort);
	
	public UserGroup finfOne(String paramName, Object paramValue);
	
	public UserGroup finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
