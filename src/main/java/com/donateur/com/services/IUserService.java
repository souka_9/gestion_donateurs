package com.donateur.com.services;

import java.util.List;

import com.donateur.com.entities.User;


public interface IUserService {
	
public User save(User entity);
	
	public User update(User entity);
	
	public List<User> selectAll();
	
	public User getByIf(Long id);
	
	public void remove(Long id);

	public List<User> selectAll(String sortField, String sort);
	
	public User finfOne(String paramName, Object paramValue);
	
	public User finfOne(String[] paramNames, Object[] paramValue);
	
	public int findCountBy(String paraName, String paramValue);

}
