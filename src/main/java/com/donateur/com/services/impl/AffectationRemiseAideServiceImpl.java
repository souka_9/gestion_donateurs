package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.IAffectationRemiseAideDao;
import com.donateur.com.entities.AffectationRemiseAide;
import com.donateur.com.services.IAffectationRemiseAideService;

@Transactional
public class AffectationRemiseAideServiceImpl implements IAffectationRemiseAideService{
	
	private IAffectationRemiseAideDao dao;
	
	public void setDao(IAffectationRemiseAideDao dao) {
		this.dao = dao;
	}

	@Override
	public AffectationRemiseAide save(AffectationRemiseAide entity) {
		return dao.save(entity);
	}

	@Override
	public AffectationRemiseAide update(AffectationRemiseAide entity) {
		return dao.update(entity);
	}

	@Override
	public List<AffectationRemiseAide> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<AffectationRemiseAide> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public AffectationRemiseAide getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public AffectationRemiseAide finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public AffectationRemiseAide finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
