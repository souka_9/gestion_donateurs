package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.IAffectationRemiseKafalaDao;
import com.donateur.com.entities.AffectationRemiseKafala;
import com.donateur.com.services.IAffectationRemiseKafalaService;

@Transactional
public class AffectationRemiseKafalaServiceImpl implements IAffectationRemiseKafalaService{
	
	private IAffectationRemiseKafalaDao dao;
	
	public void setDao(IAffectationRemiseKafalaDao dao) {
		this.dao = dao;
	}

	@Override
	public AffectationRemiseKafala save(AffectationRemiseKafala entity) {
		return dao.save(entity);
	}

	@Override
	public AffectationRemiseKafala update(AffectationRemiseKafala entity) {
		return dao.update(entity);
	}

	@Override
	public List<AffectationRemiseKafala> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<AffectationRemiseKafala> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public AffectationRemiseKafala getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public AffectationRemiseKafala finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public AffectationRemiseKafala finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
