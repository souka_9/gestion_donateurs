package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.IAvantageBeneficiaireDao;
import com.donateur.com.entities.AvantageBeneficiaire;
import com.donateur.com.services.IAvantageBeneficiaireService;

@Transactional
public class AvantageBeneficiaireServiceImpl implements IAvantageBeneficiaireService{
	
	private IAvantageBeneficiaireDao dao;
	
	public void setDao(IAvantageBeneficiaireDao dao) {
		this.dao = dao;
	}

	@Override
	public AvantageBeneficiaire save(AvantageBeneficiaire entity) {
		return dao.save(entity);
	}

	@Override
	public AvantageBeneficiaire update(AvantageBeneficiaire entity) {
		return dao.update(entity);
	}

	@Override
	public List<AvantageBeneficiaire> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<AvantageBeneficiaire> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public AvantageBeneficiaire getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public AvantageBeneficiaire finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public AvantageBeneficiaire finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
