package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.IBeneficiaireDao;
import com.donateur.com.entities.Beneficiaire;
import com.donateur.com.services.IBeneficiaireService;

@Transactional
public class BeneficiaireServiceImpl implements IBeneficiaireService{
	
	private IBeneficiaireDao dao;
	
	public void setDao(IBeneficiaireDao dao) {
		this.dao = dao;
	}

	@Override
	public Beneficiaire save(Beneficiaire entity) {
		return dao.save(entity);
	}

	@Override
	public Beneficiaire update(Beneficiaire entity) {
		return dao.update(entity);
	}

	@Override
	public List<Beneficiaire> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Beneficiaire> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public Beneficiaire getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public Beneficiaire finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public Beneficiaire finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
