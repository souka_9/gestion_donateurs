package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.services.IDonateurService;
import com.donateur.com.dao.IDonateurDao;
import com.donateur.com.entities.Donateur;


@Transactional
public class DonateurServiceImpl implements IDonateurService{
	
	private IDonateurDao dao;
	
	public void setDao(IDonateurDao dao) {
		this.dao = dao;
	}

	@Override
	public Donateur save(Donateur entity) {
		return dao.save(entity);
	}

	@Override
	public Donateur update(Donateur entity) {
		return dao.update(entity);
	}

	@Override
	public List<Donateur> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Donateur> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public Donateur getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public Donateur finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public Donateur finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
