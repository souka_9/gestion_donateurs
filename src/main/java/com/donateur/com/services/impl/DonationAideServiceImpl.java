package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.IDonationAideDao;
import com.donateur.com.entities.DonationAide;
import com.donateur.com.services.IDonationAideService;

@Transactional
public class DonationAideServiceImpl implements IDonationAideService{
	
	private IDonationAideDao dao;
	
	public void setDao(IDonationAideDao dao) {
		this.dao = dao;
	}

	@Override
	public DonationAide save(DonationAide entity) {
		return dao.save(entity);
	}

	@Override
	public DonationAide update(DonationAide entity) {
		return dao.update(entity);
	}

	@Override
	public List<DonationAide> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<DonationAide> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public DonationAide getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public DonationAide finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public DonationAide finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
