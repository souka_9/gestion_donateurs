package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.IDonationKafalaDao;
import com.donateur.com.entities.DonationKafala;
import com.donateur.com.services.IDonationKafalaService;

@Transactional
public class DonationKafalaServiceImpl implements IDonationKafalaService{
	
	private IDonationKafalaDao dao;
	
	public void setDao(IDonationKafalaDao dao) {
		this.dao = dao;
	}

	@Override
	public DonationKafala save(DonationKafala entity) {
		return dao.save(entity);
	}

	@Override
	public DonationKafala update(DonationKafala entity) {
		return dao.update(entity);
	}

	@Override
	public List<DonationKafala> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<DonationKafala> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public DonationKafala getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public DonationKafala finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public DonationKafala finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
