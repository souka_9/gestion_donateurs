package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.IEnfantEtudiantDao;
import com.donateur.com.entities.EnfantEtudiant;
import com.donateur.com.services.IEnfantEtudiantService;

@Transactional
public class EnfantEtudiantServiceImpl implements IEnfantEtudiantService{
	
	private IEnfantEtudiantDao dao;
	
	public void setDao(IEnfantEtudiantDao dao) {
		this.dao = dao;
	}

	@Override
	public EnfantEtudiant save(EnfantEtudiant entity) {
		return dao.save(entity);
	}

	@Override
	public EnfantEtudiant update(EnfantEtudiant entity) {
		return dao.update(entity);
	}

	@Override
	public List<EnfantEtudiant> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<EnfantEtudiant> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public EnfantEtudiant getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public EnfantEtudiant finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public EnfantEtudiant finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
