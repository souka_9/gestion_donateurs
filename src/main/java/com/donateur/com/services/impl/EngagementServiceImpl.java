package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.IEngagementDao;
import com.donateur.com.entities.Engagement;
import com.donateur.com.services.IEngagementService;

@Transactional
public class EngagementServiceImpl implements IEngagementService{
	
	private IEngagementDao dao;
	
	public void setDao(IEngagementDao dao) {
		this.dao = dao;
	}

	@Override
	public Engagement save(Engagement entity) {
		return dao.save(entity);
	}

	@Override
	public Engagement update(Engagement entity) {
		return dao.update(entity);
	}

	@Override
	public List<Engagement> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Engagement> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public Engagement getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public Engagement finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public Engagement finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
