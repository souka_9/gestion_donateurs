package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.IModeleReportingDao;
import com.donateur.com.entities.ModeleReporting;
import com.donateur.com.services.IModeleReportingService;

@Transactional
public class ModeleReportingServiceImpl implements IModeleReportingService{
	
	private IModeleReportingDao dao;
	
	public void setDao(IModeleReportingDao dao) {
		this.dao = dao;
	}

	@Override
	public ModeleReporting save(ModeleReporting entity) {
		return dao.save(entity);
	}

	@Override
	public ModeleReporting update(ModeleReporting entity) {
		return dao.update(entity);
	}

	@Override
	public List<ModeleReporting> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<ModeleReporting> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public ModeleReporting getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public ModeleReporting finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public ModeleReporting finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
