package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.IResultatDao;
import com.donateur.com.entities.Resultat;
import com.donateur.com.services.IResultatService;

@Transactional
public class ResultatServiceImpl implements IResultatService{
	
	private IResultatDao dao;
	
	public void setDao(IResultatDao dao) {
		this.dao = dao;
	}

	@Override
	public Resultat save(Resultat entity) {
		return dao.save(entity);
	}

	@Override
	public Resultat update(Resultat entity) {
		return dao.update(entity);
	}

	@Override
	public List<Resultat> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Resultat> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public Resultat getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public Resultat finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public Resultat finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
