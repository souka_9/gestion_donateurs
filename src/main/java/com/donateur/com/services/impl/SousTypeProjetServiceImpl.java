package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.ISousTypeProjetDao;
import com.donateur.com.entities.SousTypeProjet;
import com.donateur.com.services.ISousTypeProjetService;

@Transactional
public class SousTypeProjetServiceImpl implements ISousTypeProjetService{
	
	private ISousTypeProjetDao dao;
	
	public void setDao(ISousTypeProjetDao dao) {
		this.dao = dao;
	}

	@Override
	public SousTypeProjet save(SousTypeProjet entity) {
		return dao.save(entity);
	}

	@Override
	public SousTypeProjet update(SousTypeProjet entity) {
		return dao.update(entity);
	}

	@Override
	public List<SousTypeProjet> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<SousTypeProjet> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public SousTypeProjet getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public SousTypeProjet finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public SousTypeProjet finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
