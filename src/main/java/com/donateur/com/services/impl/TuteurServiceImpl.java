package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.ITuteurDao;
import com.donateur.com.entities.Tuteur;
import com.donateur.com.services.ITuteurService;

@Transactional
public class TuteurServiceImpl implements ITuteurService{
	
	private ITuteurDao dao;
	
	public void setDao(ITuteurDao dao) {
		this.dao = dao;
	}

	@Override
	public Tuteur save(Tuteur entity) {
		return dao.save(entity);
	}

	@Override
	public Tuteur update(Tuteur entity) {
		return dao.update(entity);
	}

	@Override
	public List<Tuteur> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Tuteur> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public Tuteur getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public Tuteur finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public Tuteur finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
