package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.ITypeDactiviteDao;
import com.donateur.com.entities.TypeDactivite;
import com.donateur.com.services.ITypeDactiviteService;

@Transactional
public class TypeDactiviteServiceImpl implements ITypeDactiviteService{
	
	private ITypeDactiviteDao dao;
	
	public void setDao(ITypeDactiviteDao dao) {
		this.dao = dao;
	}

	@Override
	public TypeDactivite save(TypeDactivite entity) {
		return dao.save(entity);
	}

	@Override
	public TypeDactivite update(TypeDactivite entity) {
		return dao.update(entity);
	}

	@Override
	public List<TypeDactivite> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<TypeDactivite> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public TypeDactivite getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public TypeDactivite finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public TypeDactivite finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
