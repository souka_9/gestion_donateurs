package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.ITypeProjetDao;
import com.donateur.com.entities.TypeProjet;
import com.donateur.com.services.ITypeProjetService;

@Transactional
public class TypeProjetServiceImpl implements ITypeProjetService{
	
	private ITypeProjetDao dao;
	
	public void setDao(ITypeProjetDao dao) {
		this.dao = dao;
	}

	@Override
	public TypeProjet save(TypeProjet entity) {
		return dao.save(entity);
	}

	@Override
	public TypeProjet update(TypeProjet entity) {
		return dao.update(entity);
	}

	@Override
	public List<TypeProjet> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<TypeProjet> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public TypeProjet getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public TypeProjet finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public TypeProjet finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
