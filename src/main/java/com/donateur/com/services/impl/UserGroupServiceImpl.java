package com.donateur.com.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.IUserGroupDao;
import com.donateur.com.entities.UserGroup;
import com.donateur.com.services.IUserGroupService;

@Transactional
public class UserGroupServiceImpl implements IUserGroupService{
	
	private IUserGroupDao dao;
	
	public void setDao(IUserGroupDao dao) {
		this.dao = dao;
	}

	@Override
	public UserGroup save(UserGroup entity) {
		return dao.save(entity);
	}

	@Override
	public UserGroup update(UserGroup entity) {
		return dao.update(entity);
	}

	@Override
	public List<UserGroup> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<UserGroup> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public UserGroup getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public UserGroup finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public UserGroup finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
