package config;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.donateur.com.dao.IUserDao;
import com.donateur.com.entities.User;
import com.donateur.com.services.IUserService;

@Transactional
public class UserServiceImpl implements IUserService{
	
	private IUserDao dao;
	
	public void setDao(IUserDao dao) {
		this.dao = dao;
	}

	@Override
	public User save(User entity) {
		return dao.save(entity);
	}

	@Override
	public User update(User entity) {
		return dao.update(entity);
	}

	@Override
	public List<User> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<User> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}
	
	@Override
	public User getByIf(Long id) {
		return dao.getByIf(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	

	@Override
	public User finfOne(String paramName, Object paramValue) {
		return dao.finfOne(paramName, paramValue);
	}

	@Override
	public User finfOne(String[] paramNames, Object[] paramValue) {
		return dao.finfOne(paramNames, paramValue);
	}

	@Override
	public int findCountBy(String paraName, String paramValue) {
		return dao.findCountBy(paraName, paramValue);
	}

}
