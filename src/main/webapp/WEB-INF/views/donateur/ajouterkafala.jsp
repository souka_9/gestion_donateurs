<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Gentelella Alela! |</title>


<!-- Bootstrap -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Font Awesome -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!-- NProgress -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/nprogress/nprogress.css"
	rel="stylesheet">

<!-- iCheck -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/iCheck/skins/flat/green.css"
	rel="stylesheet">
<!-- Datatables -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/pagination.less">

<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/mixins/grid-framework.less">
<link
	href="<%=request.getContextPath()%>/resources/build/css/custom.min">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/mixins/vendor-prefixe">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-bs/css/dataTbles.bootstrap.min">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/scaffloding.less">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/normalize.less">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/mixins/vendoe-prefixes.less">

 <!-- bootstrap-wysiwyg -->
    <link href="<%=request.getContextPath() %>/resources/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="<%=request.getContextPath() %>/resources/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="<%=request.getContextPath() %>/resources/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="<%=request.getContextPath() %>/resources/vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<%=request.getContextPath() %>/resources/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<!-- Custom Theme Style -->
<link
	href="<%=request.getContextPath()%>/resources/build/css/custom.min.css"
	rel="stylesheet">
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">

			<%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp"%>

			<!-- top navigation -->

			<%@ include file="/WEB-INF/views/menu_top/topMenu.jsp"%>

			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col" role="main">

				<div class="">
					<div class="page-title">


						<h2>
							<fmt:message key="donation.nouveau" />
						</h2>

						<div class="clearfix"></div>


						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										<h2>Formulaire d'ajout </h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i
													class="fa fa-chevron-up"></i></a></li>
											<li class="dropdown"><a href="#" class="dropdown-toggle"
												data-toggle="dropdown" role="button" aria-expanded="false"><i
													class="fa fa-wrench"></i></a>
												<ul class="dropdown-menu" role="menu">
													<li><a href="#">Settings 1</a></li>
													<li><a href="#">Settings 2</a></li>
												</ul></li>
											<li><a class="close-link"><i class="fa fa-close"></i></a>
											</li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										<br />

                                        <c:url value="/donationkafala/ajouterkafala" var = "urlEnregister"/>
										<f:form modelAttribute="donationkafala" class="form-horizontal form-label-left"  action="${urlEnregister }">
                                            
											

											
											
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message
														key="common.typekafala" /></label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<f:select class="form-control" path="typeKafala">
														<option><fmt:message key="common.general" /></option>
														<option><fmt:message key="common.etudiant" /></option>
														<option><fmt:message key="common.scolaire" /></option>
													</f:select>
												</div>
											</div>
											
											
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message
														key="common.typepaiement"/></label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<f:select class="form-control" path="typePaiement">
														<option><fmt:message key="common.cheque" /></option>
														<option><fmt:message key="common.virment" /></option>
														<option><fmt:message key="common.espece" /></option>
													</f:select>
												</div>
											</div>
											
											
											<div class="form-group">
												<label for="montant"
													class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message
														key="common.montant" /></label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<f:input class="form-control col-md-7 col-xs-12" type="text"
														name="montant" path="montant" />
												</div>
											</div>
											
											
											<div class="ln_solid"></div>
											<div class="form-group">
												<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
													<a href="<c:url value="/donation/"/>" class="btn btn-danger">
														<i class="fa fa-arrow-left">&nbsp;<fmt:message
																key="common.annuler" /></i>
													</a>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-arrow-leftfa fa-save">&nbsp;<fmt:message
																key="common.enregistrer" /></i>
													</button>
												</div>
											</div>

										</f:form>
									</div>
								</div>
							</div>
						</div>



					</div>
				</div>
			</div>
			<!-- /page content -->

			<!-- footer content -->
			<footer>
				<div class="pull-right">
					<a
						href="https://colorlib.com"></a>
				</div>
				<div class="clearfix"></div>
			</footer>
			<!-- /footer content -->
		</div>
	</div>

	<!-- jQuery -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/nprogress/nprogress.js"></script>
	<!-- iCheck -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/iCheck/icheck.min.js"></script>
		<!-- bootstrap-daterangepicker -->
    <script src="<%=request.getContextPath() %>/resources/vendors/moment/min/moment.min.js"></script>
    <script src="<%=request.getContextPath() %>/resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="<%=request.getContextPath() %>/resources/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="<%=request.getContextPath() %>/resources/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="<%=request.getContextPath() %>/resources/vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="<%=request.getContextPath() %>/resources/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="<%=request.getContextPath() %>/resources/vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="<%=request.getContextPath() %>/resources/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="<%=request.getContextPath() %>/resources/vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="<%=request.getContextPath() %>/resources/vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="<%=request.getContextPath() %>/resources/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="<%=request.getContextPath() %>/resources/vendors/starrr/dist/starrr.js"></script>
		
	<!-- Datatables -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/jszip/dist/jszip.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/pdfmake/build/pdfmake.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/pdfmake/build/vfs_fonts.js"></script>
	<!-- Custom Theme Scripts -->
	<script
		src="<%=request.getContextPath()%>/resources/build/js/custom.min.js"></script>
</body>
</html>
