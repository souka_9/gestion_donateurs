<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Gentelella Alela! |</title>

<!-- Bootstrap -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Font Awesome -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!-- NProgress -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/nprogress/nprogress.css"
	rel="stylesheet">

<!-- iCheck -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/iCheck/skins/flat/green.css"
	rel="stylesheet">
<!-- Datatables -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/pagination.less">

<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/mixins/grid-framework.less">
<link
	href="<%=request.getContextPath()%>/resources/build/css/custom.min">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/mixins/vendor-prefixe">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-bs/css/dataTbles.bootstrap.min">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/scaffloding.less">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/normalize.less">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/mixins/vendoe-prefixes.less">

<!-- Custom Theme Style -->
<link
	href="<%=request.getContextPath()%>/resources/build/css/custom.min.css"
	rel="stylesheet">

<style>
th {
	background-color: #1ABB9C;
	color: white;
}
</style>

</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">

			<%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp"%>

			<!-- top navigation -->

			<%@ include file="/WEB-INF/views/menu_top/topMenu.jsp"%>

			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">

						<h3>
							Liste des engagements
						</h3>
						<div>

							<div class="" role="tabpanel" data-example-id="togglable-tabs">
								<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
									<li><a href="<c:url value="/engagement/nouveau"/>"
										class="fa fa-plus">&nbsp;<fmt:message key="common.ajouter" /></a></li>
									
									
								</ul>

							</div>


						</div>

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>
										
									</h2>
									<ul class="nav navbar-right panel_toolbox">
										<li><a class="collapse-link"><i
												class="fa fa-chevron-up"></i></a></li>
										<li class="dropdown"><a href="#" class="dropdown-toggle"
											data-toggle="dropdown" role="button" aria-expanded="false"><i
												class="fa fa-wrench"></i></a>
											<ul class="dropdown-menu" role="menu">
												<li><a href="#">Settings 1</a></li>
												<li><a href="#">Settings 2</a></li>
											</ul></li>
										<li><a class="close-link"><i class="fa fa-close"></i></a>
										</li>
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<table id="datatable"
										class="table table-striped table-bordered">
										<thead>
											<tr>
										     	<th><fmt:message key="common.nomPrenomContact" /></th>
												<th><fmt:message key="common.montant" /></th>
												<th><fmt:message key="common.nbrBenif" /></th>
												<th><fmt:message key="common.typessprojet" /></th>
											</tr>
										</thead>


										<tbody>

											<c:forEach items="${engagements }" var="engagement">
											
												<tr>
													<td><input ></td>
													<td>${engagement.getMontantMensuel() }</td>
													<td>${engagement.getNmbrBenef() }</td>
													<td>${engagement.getTypeSsProjet() }</td>
													
													<td>
													<c:url value="/engagement/modifier/${engagement.getIdengagement() }" var="urlModif" />
													 <a href="${urlModif }"><i class="fa fa-edit"></i></a>
													  &nbsp;|&nbsp; 
													  
													  
														<a href="javascript:void(0);" data-toggle="modal" data-target="#modalEngagement${engagement.getIdengagement() }">
															<i class="fa fa-trash-o"></i></a>
														<div class="modal fade bs-example-modal-sm" id="modalEngagement${engagement.getIdengagement() }" tabindex="-1" role="dialog" aria-hidden="true">
															<div class="modal-dialog modal-sm">
																<div class="modal-content">
																	<div class="modal-header">
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
																	<h4 class="modal-title" id="myModalLabel2"><fmt:message key="common.confirm.suppression" /></h4>
																	</div>
																	<div class="modal-body">
																		<h4><fmt:message key="engagement.confirm.suppression.msg" /></h4>
																	</div>
																	<div class="modal-footer">
																		<button type="button" class="btn btn-default"
																			data-dismiss="modal"><fmt:message
														key="common.reset" /></button>
																		<c:url value="/engagement/supprimer/${engagement.getIdengagement() }" var="urlSuppression"/>
																		<a href="${urlSuppression }" class="btn btn-danger">
																			<i class="fa fa-trash-o"></i>&nbsp;<fmt:message
														key="common.confirmer" />
																		</a>
																	</div>

																</div>
															</div>
														</div>
														</td>
												</tr>

											</c:forEach>

										</tbody>
									</table>
								</div>
							</div>
						</div>

					</div>

				</div>
			</div>
			<!-- /page content -->

			<!-- footer content -->
			<footer>
				<div class="pull-right">
					 <a
						href="https://colorlib.com"></a>
				</div>
				<div class="clearfix"></div>
			</footer>
			<!-- /footer content -->
		</div>
	</div>

	<!-- jQuery -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/nprogress/nprogress.js"></script>
	<!-- iCheck -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/iCheck/icheck.min.js"></script>
	<!-- Datatables -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/jszip/dist/jszip.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/pdfmake/build/pdfmake.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/pdfmake/build/vfs_fonts.js"></script>
	<!-- Custom Theme Scripts -->
	<script
		src="<%=request.getContextPath()%>/resources/build/js/custom.min.js"></script>
</body>
</html>
