<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="<%=request.getContextPath() %>/resources/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=request.getContextPath() %>/resources/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<%=request.getContextPath() %>/resources/vendors/nprogress/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<%=request.getContextPath() %>/resources/build/css/custom.min.css" rel="stylesheet">
    
    <style type="text/css">

h1
{
  font-size: 48px;
  text-shadow: 2px 2px 3px #000000;
  font-family:"Segoe print", Arial, Helvetica, sans-serif;
  color: #24C887;
  padding:64px 0 0 0;
  margin:16px auto;
  

  text-align:center;
  display:block;
 
}

    </style>
    
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        
        <%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp" %>

        <!-- top navigation -->
      
       <%@ include file="/WEB-INF/views/menu_top/topMenu.jsp" %>
       
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
            
           <div class="center"> <h1>Bienvenue sur votre application gestion donateurs</h1></div>
            
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">
                
              </div>
            </div>
             <div class="center">
				<img src="<%=request.getContextPath() %>/resources/images/trans.png" alt="..." style="width:45%;" class="center">
			</div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            <a href="https://colorlib.com"></a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<%=request.getContextPath() %>/resources/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<%=request.getContextPath() %>/resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<%=request.getContextPath() %>/resources/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<%=request.getContextPath() %>/resources/vendors/nprogress/nprogress.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="<%=request.getContextPath() %>/resources/build/js/custom.min.js"></script>
  </body>
</html>
