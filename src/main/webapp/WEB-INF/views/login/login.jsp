<%@ include file="/WEB-INF/views/includes/includes.jsp"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Gentelella Alela! |</title>

<!-- Bootstrap -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Font Awesome -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!-- NProgress -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/nprogress/nprogress.css"
	rel="stylesheet">
<!-- Animate.css -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/animate.css/animate.min.css"
	rel="stylesheet">

<!-- Custom Theme Style -->
<link
	href="<%=request.getContextPath()%>/resources/build/css/custom.min.css"
	rel="stylesheet">

<style>
body {
	background-color: #F7F7F7;
}

h1 {
	color: #AF1C1C;
}

form {
	border-style: dashed;
	border-color: #4D23AF;
}

a.five {
	border-style: solid;
	border-color: #0EA468;
}
</style>

</head>

<body class="text-center">
	<img src="<%=request.getContextPath()%>/resources/images/trans.png"
		class="center" alt="..." width="700" height="200">

	<div class="text-center"></div>
	<div>


		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">

					<form>
						<h1>Authentification</h1>
						<div>
							<input type="text" class="form-control" class="fa fa-user"
								placeholder="Username" required="" />
						</div>
						<div>
							<input type="password" class="form-control" class="fa fa-lock"
								placeholder="Password" required="" />
						</div>
						<div class="center">
						<c:url value="/home/" var="home" />
							<a class="btn btn-default submit" href="${home }" 
								style="border-style: solid; border-color: #0EA468;">S'identifier</a>
							<a class="reset_pass" href="#"></a>
							
						
						</div>

						
							
						
						<div class="clearfix"></div>

						<div class="separator">
							<p class="change_link">
								<a href="#signup" class="to_register"> </a>
							</p>

							<div class="clearfix"></div>
							<br />

							<div>

								<p></p>
							</div>
						</div>
					</form>
				</section>
			</div>

			<div id="register" class="animate form registration_form">
				<section class="login_content">
					<form>
						<h1>Create Account</h1>
						<div>
							<input type="text" class="form-control" placeholder="Username"
								required="" />
						</div>
						<div>
							<input type="email" class="form-control" placeholder="Email"
								required="" />
						</div>
						<div>
							<input type="password" class="form-control"
								placeholder="Password" required="" />
						</div>
						<div>
							<a class="btn btn-default submit" href="index.html">Submit</a>
						</div>

						<div class="clearfix"></div>

						<div class="separator">
							<p class="change_link">
								D�j� membre ? <a href="#signin" class="to_register">
									S'identifier </a>
							</p>

							<div class="clearfix"></div>
							<br />

							<div>
								<h1>
									<i class="fa fa-paw"></i> Gestion Donateurs
								</h1>
								<p></p>
							</div>
						</div>
					</form>
				</section>
			</div>
		</div>
	</div>
</body>
</html>






