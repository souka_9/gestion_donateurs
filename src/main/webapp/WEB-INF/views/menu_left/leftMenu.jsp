
<div class="col-md-3 left_col">
	<div class="left_col scroll-view">

		<div class="clearfix"></div>

		<!-- menu profile quick info -->
		<div class="profile clearfix">
			<div >
				<img src="<%=request.getContextPath() %>/resources/images/trans.png" alt="..." class="img-circle profile_img">
			</div>
			<div class="profile_info">
				
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- /menu profile quick info -->
		<br />
		<!-- sidebar menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
<!-- 				<h3> -->
<!-- 					<fmt:message key="common.dashbord" /> -->
<!-- 				</h3> -->
				<ul class="nav side-menu">
				    <c:url value="/home/" var="home" />
					<li><a href="${home }"><i class="fa fa-home"></i> <fmt:message key="common.acceuil" />
					     <span class="fa fa-chevron-down"></span></a></li>
<!-- user -->
                    
					<li><a><i class="fa fa-female"></i> <fmt:message key="common.user" /> <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
						<c:url value="/user/" var="user" />
							<li><a href="${user }"><fmt:message key="common.user.utilisateur" /></a></li>
						</ul></li>
						
<!--donateur-->		
					<li><a><i class="fa fa-user"></i> <fmt:message key="common.donateur" /> <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
						<c:url value="/donateur/" var="donateur" />
							<li><a href="${donateur }"><fmt:message key="common.donateur.donneur" /></a></li>
						<c:url value="/engagement/" var="engagement" />
							<li><a href="${engagement }"><fmt:message key="common.donateur.engagement" /></a></li>		
						</ul></li>
						
<!--beneficiaire-->					
						
					<li><a><i class="fa fa-group"></i> <fmt:message key="common.beneficiaire" /> <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
						<c:url value="/tuteur/" var="tuteur" />
							<li><a href="${tuteur }"><fmt:message key="common.beneficiaire.tuteur" /></a></li>
						<c:url value="/enfantetudiant/" var="enfantetudiant" />
							<li><a href="${enfantetudiant }"><fmt:message key="common.beneficiaire.enfant" /></a></li>
						</ul></li>
						
<!--typeprojet-->
                    <li><a><i class="fa fa-desktop"></i> <fmt:message key="common.typeprojet" /> <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
						<c:url value="/typeprojetaide/" var="typeprojetaide" />
							<li><a href="${typeprojetaide }"><fmt:message key="common.typeprojet.aide" /></a></li>
						<c:url value="/typeprojetkafala/" var="typeprojetkafala" />
							<li><a href="${typeprojetkafala }"><fmt:message key="common.typeprojet.kafala" /></a></li>
						</ul></li>

<!--donation-->

                    <li><a><i class="fa fa-heart"></i> <fmt:message key="common.donation" /> <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
						<c:url value="/donationaide/" var="donationaide" />
							<li><a href="${donationaide }"><fmt:message key="common.donation.aide" /></a></li>
						<c:url value="/donationkafala/" var="donationkafala" />
							<li><a href="${donationkafala }"><fmt:message key="common.donation.kafala" /></a></li>
						</ul></li>

<!--mdlreporting-->           
					
					<li><a><i class="fa fa-file-text"></i> <fmt:message key="common.mdlreporting" /> <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
						<c:url value="/mdlreporting/" var="mdlreporting" />
							<li><a href="${mdlreporting }"><fmt:message key="common.mdlreporting" /></a></li>
						</ul></li>
						
<!--statistique-->           
						
					<li><a><i class="fa fa-bar-chart-o"></i> <fmt:message key="common.statistique" /> <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
						<c:url value="/statistique/" var="statistique" />
							<li><a href="${statistique }"><fmt:message key="common.statistique" /></a></li>
						</ul></li>	
				
				</ul>
			</div>

		</div>
		<!-- /sidebar menu -->

		<!-- /menu footer buttons -->
		<div class="sidebar-footer hidden-small">
			<a data-toggle="tooltip" data-placement="top" title="Settings"> <span
				class="glyphicon glyphicon-cog" aria-hidden="true"></span>
			</a> <a data-toggle="tooltip" data-placement="top" title="FullScreen">
				<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
			</a> <a data-toggle="tooltip" data-placement="top" title="Lock"> <span
				class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
			</a> 
			
			<a data-toggle="tooltip" data-placement="top" title="Logout"
				href="<c:url value = "/" var="login" />"> <span class="glyphicon glyphicon-off"
				aria-hidden="true"></span>
			</a>
		</div>
		<!-- /menu footer buttons -->
	</div>
</div>