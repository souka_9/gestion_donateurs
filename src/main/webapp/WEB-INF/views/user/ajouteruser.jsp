<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Gentelella Alela! |</title>


<!-- Bootstrap -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Font Awesome -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!-- NProgress -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/nprogress/nprogress.css"
	rel="stylesheet">

<!-- iCheck -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/iCheck/skins/flat/green.css"
	rel="stylesheet">
<!-- Datatables -->
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/pagination.less">

<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/mixins/grid-framework.less">
<link
	href="<%=request.getContextPath()%>/resources/build/css/custom.min">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/mixins/vendor-prefixe">
<link
	href="<%=request.getContextPath()%>/resources/vendors/datatables.net-bs/css/dataTbles.bootstrap.min">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/scaffloding.less">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/normalize.less">
<link
	href="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/css/less/mixins/vendoe-prefixes.less">

<!-- Custom Theme Style -->
<link
	href="<%=request.getContextPath()%>/resources/build/css/custom.min.css"
	rel="stylesheet">
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">

			<%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp"%>

			<!-- top navigation -->

			<%@ include file="/WEB-INF/views/menu_top/topMenu.jsp"%>

			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col" role="main">

				<div class="">
					<div class="page-title">

						
						<h2>
							<fmt:message key="user.nouveau" />
						</h2>

						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										<h2>Formulaire d'inscription</h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i
													class="fa fa-chevron-up"></i></a></li>
											<li class="dropdown"><a href="#" class="dropdown-toggle"
												data-toggle="dropdown" role="button" aria-expanded="false"><i
													class="fa fa-wrench"></i></a>
												<ul class="dropdown-menu" role="menu">
													<li><a href="#">Settings 1</a></li>
													<li><a href="#">Settings 2</a></li>
												</ul></li>
											<li><a class="close-link"><i class="fa fa-close"></i></a>
											</li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										<br />
										<c:url value="/user/enregistrer" var = "urlEnregistrer"/>
										<f:form class="form-horizontal form-label-left input_mask"
											modelAttribute="user" action="${urlEnregistrer }" >

											<div
												class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
												<f:input type="text" class="form-control has-feedback-left"
													id="inputSuccess2" placeholder="prenom"
													 path="prenom"/>
													<f:hidden path="iduser"/> 
													<span class="fa fa-user form-control-feedback left"
													aria-hidden="true"></span>
											</div>

											<div
												class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
												<f:input type="text" class="form-control" id="inputSuccess3"
													placeholder="nom" path="nom"/> <span
													class="fa fa-user form-control-feedback right"
													aria-hidden="true"></span>
											</div>

											<div
												class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
												<f:input type="text" class="form-control has-feedback-left"
													id="inputSuccess4" placeholder="email" path="email"/> <span
													class="fa fa-envelope form-control-feedback left"
													aria-hidden="true"></span>
											</div>

											<div
												class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
												<f:input type="text" class="form-control" id="inputSuccess5"
													placeholder="tel" path="tel"/>
												<span class="fa fa-phone form-control-feedback right"
													aria-hidden="true"></span>
											</div>

											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message
														key="common.login" /></label>
												<div class="col-md-9 col-sm-9 col-xs-12">
													<f:input type="text" class="form-control"
														placeholder="login" path="login"/>
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message
														key="common.password" /></label>
												<div class="col-md-9 col-sm-9 col-xs-12">
													<f:input type="text" class="form-control"
														placeholder="password" path="password"/>
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message
														key="common.group" /></label>
												<div class="col-md-9 col-sm-9 col-xs-12">
													<f:select class="form-control" path="groupe">
														<option><fmt:message key="common.admin" /></option>
														<option><fmt:message key="common.agent" /></option>
														<option><fmt:message key="common.admin_techniq" /></option>
														<option><fmt:message key="common.g�rant_ref" /></option>
													</f:select>
												</div>
											</div>
											<div class="ln_solid"></div>
											<div class="form-group">
												<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
													<a href= "<c:url value="/user/"/>" class="btn btn-danger"><i class="fa fa-arrow-left">&nbsp;<fmt:message key="common.annuler"/></i></a>
													<button type="submit" class="btn btn-success"><i class="fa fa-arrow-leftfa fa-save">&nbsp;<fmt:message key="common.enregistrer"/></i></button>
												</div>
											</div>

										</f:form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /page content -->

			<!-- footer content -->
			<footer>
				<div class="pull-right">
					 <a
						href="https://colorlib.com"></a>
				</div>
				<div class="clearfix"></div>
			</footer>
			<!-- /footer content -->
		</div>
	</div>

	<!-- jQuery -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/nprogress/nprogress.js"></script>
	<!-- iCheck -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/iCheck/icheck.min.js"></script>
	<!-- Datatables -->
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/jszip/dist/jszip.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/pdfmake/build/pdfmake.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendors/pdfmake/build/vfs_fonts.js"></script>
	<!-- Custom Theme Scripts -->
	<script
		src="<%=request.getContextPath()%>/resources/build/js/custom.min.js"></script>
</body>
</html>
